# Soft hyphen
Use soft hyphen (&shy;) HTML tag in Text (plain) and Text (plain, long)
fields (eg. title).

## Usage on UI
1. Create your field (in type of Text (plain) or Text (plain, long)
2. Check Enable soft hyphen checkbox on Manage display tab on given field
Plain text field formatter setting.
3. Fill your field data like this:
Lorem&shy;Ipsumissimply dummy&shy;textoftheprinting and typesetting industry.

## Usage on twig file
You can use shy twig filter in twig file ```twig {{ field_name|shy }} ```
