<?php

namespace Drupal\soft_hyphen;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Custom filter for shy charachter in plain text.
 */
class ShyTwigFilter extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    $safe = [
      'is_safe' => ['html'],
    ];

    return [
      new TwigFilter('shy', [$this, 'shy'], $safe),
    ];
  }

  /**
   * Convert text to html safe except shy character.
   *
   * @param array|string $text
   *   Text to filter.
   *
   * @return string
   *   Filtered text.
   *
   * @code
   *   {{ foo|shy }}
   * @endcode
   */
  public function shy($text): string {
    $string = '';
    if (is_array($text) && isset($text['#context']['value'])) {
      $string = $text['#context']['value'];
    }
    else {
      $string = $text;
    }

    $string = htmlspecialchars($string, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8');
    return str_replace('&amp;shy;', '&shy;', $string);
  }

}
